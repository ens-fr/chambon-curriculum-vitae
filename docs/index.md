# Franck CHAMBON

<center>
_Version en ligne du CV, plus complète_

[Professeur agrégé de mathématiques](https://ens-fr.gitlab.io/chambon-curriculum-vitae/){ .md-button }
</center>

!!! info inline end "Photo"
    ![](photo.png)

Né à Guéret, dans la Creuse, le 05 décembre 1976

📧 : <franck.chambon@ac-aix-marseille.fr>

Actuellement en poste au lycée Aubrac à Bollène - Vaucluse

Domicilié dans le village voisin de Rochegude - Drôme

## Formation

- 1987-1994 : Scolarité à Ussel en Corrèze, BAC série C, mention AB
- 1994-1997 : Maths sup, math spé M, puis MP à Limoges
    - major en maths, physique et chime en fin de spé.
- 1998 - Licence de Mathématiques à l'Université de Limoges, mention TB
- 1999 - Maitrise de Mathématiques Pures à l'Université de Bordeaux I, mention B
- 2002 - CAPES de Mathématiques
- 2014 - Agrégation externe de Mathématiques
- Depuis 2010, utilisation de LaTeX, Python et autres logiciels libres.
- DIU CCIE entamé (Compétences Complémentaires en Informatique pour l'Enseignement), et poursuivi par le DIU EIL (Enseignement de l'Informatique au Lycée), ceci pour enseigner NSI.

## Enseignements

- Étés 1994-2005 : Moniteur de Voile, ou d'Escalade, pour un public adolescent
- 2002 - présent : Enseignant de Mathématiques dans des structures variées ; collège urbain délicat, lycée professionnel, collège rural, lycée général et technologique
- 2017 - présent : Enseignement de l'ICN, puis SNT en seconde, ainsi que NSI en première et terminale

## Activités périscolaires

Depuis 2010, suite à l'apprentissage de l'algorithmique en compétition, je suis devenu membre du bureau éditorial de <spoj.com>, la plus grande banque mondiale de problèmes d'algorithmique. Avant cela, j'étais devenu un auteur de problèmes reconnu, certains utilisés dans des concours de recrutement pour de grandes écoles.

Depuis 2020, je suis modérateur sur le forum national des enseignants de NSI, où mes contributions sous forme de cours et d'exercices en ligne sont appréciées. J'ai rassemblé une équipe d'enseignants solides pour une création d'exercices de qualité, relus et testés, j'en suis l'auteur principal.

J'apprécie de pouvoir construire mes figures en images vectorielles, gage de qualité et de légèreté. J'utilisais souvent Asymptote, désormais plus souvent DrawSVG qui est un module Python.

## Activités parascolaires

- Pratiques très occasionnelles de la montagne : un peu de ski de randonnée, un peu d'alpinisme, un peu de kayak en eau vive, un peu de parapente. Je fais partie du Club Montagne de Pierrelatte depuis 2004.
- Pratique désormais occasionnelle du windsurf, mais avant en compétition. Je fais partie de Pierrelatte Windsurf Association depuis sa création.
- Pratique régulière du vélo sous différentes formes : un peu de route, souvent VTT plutôt enduro, et un peu de DH. Je roule majoritairement dans le Nord-Vaucluse et le Sud-Drôme avec les copains, les copains de copains.
