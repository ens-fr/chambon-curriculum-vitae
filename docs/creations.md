# Créations récentes

Alors formateur académique en Python avec le groupe E-Mathice, j'ai donné une conférence en mars 2020 pour les enseignants de mathématiques sur le thème du dénombrement, des liens possibles avec Python. J'ai rassemblé mes notes sur un site statique écrit avec MkDocs : <https://ens-fr.gitlab.io/enumeration/>

Cette présentation a eu du succès auprès des enseignants de NSI, j'ai alors écrit un tutoriel à destination à la fois des élèves et des enseignants : <https://ens-fr.gitlab.io/mkdocs/>

Nous sommes, depuis, nombreux à utiliser cette technologie de construction de sites web statiques qui allie la simplicité de Markdown et les possibilités de script de Python.

## Informatique

Travail réalisé essentiellement en 2020 et 2021

- SNT : <https://ens-fr.gitlab.io/algo0/>
- NSI-1 : <https://ens-fr.gitlab.io/algo1/>
- NSI-2 : <https://ens-fr.gitlab.io/algo2/>

Pour les collègues, une présentation d'outils numériques : <https://lyc-84-bollene.gitlab.io/chambon/>

## Mathématiques

Pour des élèves de secondes, quelques questions autour de Python

- <https://ens-fr.gitlab.io/flash/>

Pour des élèves de Spécialité Maths en première

- <https://ens-fr.gitlab.io/math1/>

## En équipe

Travail réalisé en 2022

Grâce au travail de Vincent Bouillot, son intégration de Pyodide dans MkDocs, nous pouvons proposer des exercices en ligne corrigés. Strictement aucune donnée personnelle ne transite, aucune installation n'est nécessaire, le site est utilisable sur téléphone, tablette ou PC. Utilisation exclusive de logiciels libres, le contenu est lui-aussi sous licence libre.

- [Les exercices en cours de validation](https://e-nsi.gitlab.io/nsi-pratique/)
- [Les exercices validés](https://e-nsi.gitlab.io/pratique/)

Trois exemples avec figures

- [Sortie de labyrinthe](https://e-nsi.gitlab.io/pratique/N2/810-labyrinthe/sujet/)
- [Arbres binaires et nombres de Catalan](https://e-nsi.gitlab.io/pratique/N2/850-nb_catalan_2/sujet/)
- [Chemins et nombres de Schröder](https://e-nsi.gitlab.io/nsi-pratique/N2/nb_schroder/sujet/)
